﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Juego1_Controller : MonoBehaviour
{
    public GameObject A, B, C, D, Phrase, Mensajes;
    public int lastSelectedButton = -1;

    Word[] wordz;
    int wordz_size;
    int max_wordz_per_level;
    int current_question;
    Question[] questions_on_level;
    int answer;

    int points;

    void Awake()
    {
        string response = JsonHelper.fixJson(PlayerPrefs.GetString("ApiResponse"), "words");
        wordz = JsonHelper.FromJson<Word>(response);

        wordz_size = wordz.Length;
        max_wordz_per_level = 10;
        current_question = 0;
        points = 0;

        questions_on_level = new Question[max_wordz_per_level];

        for (int i = 0; i < max_wordz_per_level; i++)
        {
            questions_on_level[i] = new Question();

            int r = Random.Range(1, wordz_size-2);
            int r2 = Random.Range(0, 3);

            Word tmp = wordz[r];
            Word tmp_1 = wordz[r+1];
            Word tmp_2 = wordz[r-1];
            Word tmp_3 = wordz[r+2];

            switch(r2)
            {
                case 0:
                    questions_on_level[i].A = tmp.word;
                    questions_on_level[i].B = tmp_1.word;
                    questions_on_level[i].C = tmp_2.word;
                    questions_on_level[i].D = tmp_3.word;
                    break;
                case 1:
                    questions_on_level[i].A = tmp_1.word;
                    questions_on_level[i].B = tmp.word;
                    questions_on_level[i].C = tmp_2.word;
                    questions_on_level[i].D = tmp_3.word;
                    break;
                case 2:
                    questions_on_level[i].A = tmp_1.word;
                    questions_on_level[i].B = tmp_2.word;
                    questions_on_level[i].C = tmp.word;
                    questions_on_level[i].D = tmp_3.word;
                    break;
                case 3:
                    questions_on_level[i].A = tmp_1.word;
                    questions_on_level[i].B = tmp_2.word;
                    questions_on_level[i].C = tmp_3.word;
                    questions_on_level[i].D = tmp.word;
                    break;
                default:
                    questions_on_level[i].A = tmp.word;
                    questions_on_level[i].B = tmp_1.word;
                    questions_on_level[i].C = tmp_2.word;
                    questions_on_level[i].D = tmp_3.word;
                    break;
            }

            questions_on_level[i].answer = r2;
            questions_on_level[i].resource = tmp.phrase;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Mensajes.GetComponent<TextMeshProUGUI>().text = "Puntos = " + points;
        this.NextQuestion();
    }

    void NextQuestion()
    {
        if (current_question == max_wordz_per_level)
        {
            GoToGames();
            return;
        }

        A.GetComponentInChildren<TextMeshProUGUI>().text = questions_on_level[current_question].A;
        B.GetComponentInChildren<TextMeshProUGUI>().text = questions_on_level[current_question].B;
        C.GetComponentInChildren<TextMeshProUGUI>().text = questions_on_level[current_question].C;
        D.GetComponentInChildren<TextMeshProUGUI>().text = questions_on_level[current_question].D;

        answer = questions_on_level[current_question].answer;
        Phrase.GetComponentInChildren<TextMeshProUGUI>().text = questions_on_level[current_question].resource;

        current_question++;
    }

    public void ButtonWord(int option)
    {
        this.lastSelectedButton = option;
    }

    public void ButtonGo()
    {
        if (lastSelectedButton < 0)
        {
            //JCGE: ??
        }
        else
        {
            if (lastSelectedButton == answer)
                points++;

            Mensajes.GetComponent<TextMeshProUGUI>().text = "Puntos = " + points;
        }
        this.lastSelectedButton = -1;
        this.NextQuestion();
    }

    public void GoToGames()
    {
        if (PlayerPrefs.HasKey("Juego1_points"))
        {
            if (PlayerPrefs.GetInt("Juego1_points") < points)
            {
                PlayerPrefs.SetInt("Juego1_points", points);
            }
        }
        else
        {
            PlayerPrefs.SetInt("Juego1_points", points);
        }

        SaveAcumulado();
        SceneManager.LoadScene("Scene1");
    }

    public void SaveAcumulado()
    {
        if (PlayerPrefs.HasKey("Juego1_acumulado"))
        {
            PlayerPrefs.SetInt("Juego1_acumulado", PlayerPrefs.GetInt("Juego1_acumulado") + points);
        }
        else
        {
            PlayerPrefs.SetInt("Juego1_acumulado", points);
        }
    }
}

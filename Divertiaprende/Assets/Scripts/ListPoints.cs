﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ListPoints : MonoBehaviour
{
    int acumulado_points, ultimo_points;
    public GameObject title, acumulado, ultimo;
    int current_juego;

    // Start is called before the first frame update
    void Start()
    {
        current_juego = 1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetPoints(int juego)
    {
        acumulado_points = fetchAcumulado(juego);
        ultimo_points = fetchPoints(juego);

        title.GetComponent<TextMeshProUGUI>().text = "Juego: " + juego;
        acumulado.GetComponent<TextMeshProUGUI>().text = "Acumulado: " + acumulado_points;
        ultimo.GetComponent<TextMeshProUGUI>().text = "Ultimo: " + ultimo_points;
    }

    public int fetchPoints(int juego)
    {
        int points = 0;

        if (!PlayerPrefs.HasKey("Juego"+ juego +"_points"))
        {
            PlayerPrefs.SetInt("Juego" + juego + "_points", points);
        }

        points = PlayerPrefs.GetInt("Juego" + juego + "_points");
        return points;
    }


    public int fetchAcumulado(int juego)
    {
        int points = 0;

        if (!PlayerPrefs.HasKey("Juego" + juego + "_acumulado"))
        {
            PlayerPrefs.SetInt("Juego" + juego + "_acumulado", points);
        }

        points = PlayerPrefs.GetInt("Juego" + juego + "_acumulado");
        return points;
    }
}

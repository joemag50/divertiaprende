﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class FetchApi : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("ApiResponse"))
        {
            //JCGE: ??
        }
        else
        {
            StartCoroutine(FetchDivertiAprendeApi());
        }
    }

    IEnumerator FetchDivertiAprendeApi()
    {
        UnityWebRequest www = UnityWebRequest.Get("https://divertiaprende-api.herokuapp.com/v1/all");
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            //JCGE: Error
        }
        else
        {
            PlayerPrefs.SetString("ApiResponse", www.downloadHandler.text);

            // Or retrieve results as binary data
            byte[] results = www.downloadHandler.data;
        }
    }
}

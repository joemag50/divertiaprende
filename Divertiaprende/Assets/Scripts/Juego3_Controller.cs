﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Juego3_Controller : MonoBehaviour
{
    public GameObject A, B, C, D, Question, Mensajes;
    public int lastSelectedButton = -1;

    int times_per_game;
    int current_times;
    int points;
    bool esSuma;

    int Number1;
    int Number2;

    int r;
    // Start is called before the first frame update
    void Start()
    {
        times_per_game = 10;
        current_times = 0;

        this.SetLabels();
    }

    void SetLabels()
    {
        if (current_times == times_per_game)
        {
            this.GoToGames();
            return;
        }

        Number1 = Random.Range(1, 30);
        Number2 = Random.Range(1, 30);
        if (Number1 == Number2)
        {
            Number2++;
        }
        int rand_suma = Random.Range(0, 2);
        r = Random.Range(0, 4);

        if (rand_suma == 1)
            esSuma = true;
        else
            esSuma = false;

        switch (r)
        {
            case 0:
                A.GetComponentInChildren<TextMeshProUGUI>().text = "" + (Number1 + Number1);
                B.GetComponentInChildren<TextMeshProUGUI>().text = "" + (Number1 - Number2);
                C.GetComponentInChildren<TextMeshProUGUI>().text = "" + (Number2 + Number2);
                D.GetComponentInChildren<TextMeshProUGUI>().text = "" + (Number2 + Number1);
                break;
            case 1:
                A.GetComponentInChildren<TextMeshProUGUI>().text = "" + (Number2 + Number1);
                B.GetComponentInChildren<TextMeshProUGUI>().text = "" + (Number1 + Number1);
                C.GetComponentInChildren<TextMeshProUGUI>().text = "" + (Number1 - Number2);
                D.GetComponentInChildren<TextMeshProUGUI>().text = "" + (Number2 + Number2);
                break;
            case 2:
                A.GetComponentInChildren<TextMeshProUGUI>().text = "" + (Number1 - Number2);
                B.GetComponentInChildren<TextMeshProUGUI>().text = "" + (Number2 + Number2);
                C.GetComponentInChildren<TextMeshProUGUI>().text = "" + (Number2 + Number1);
                D.GetComponentInChildren<TextMeshProUGUI>().text = "" + (Number1 + Number1);
                break;
            case 3:
                A.GetComponentInChildren<TextMeshProUGUI>().text = "" + (Number1 + Number1);
                B.GetComponentInChildren<TextMeshProUGUI>().text = "" + (Number1 - Number2);
                C.GetComponentInChildren<TextMeshProUGUI>().text = "" + (Number2 + Number2);
                D.GetComponentInChildren<TextMeshProUGUI>().text = "" + (Number2 + Number1);
                break;
            default:
                break;
        }

        if (esSuma)
            Question.GetComponentInChildren<TextMeshProUGUI>().text = Number1 + " + " + Number2;
        else
            Question.GetComponentInChildren<TextMeshProUGUI>().text = Number1 + " - " + Number2;
    }

    public void ButtonWord(int option)
    {
        this.lastSelectedButton = option;
    }

    public void ButtonGo()
    {
        if (lastSelectedButton < 0)
        {
            //JCGE: ??
        }
        else
        {
            switch (r)
            {
                case 0:
                    if (esSuma)
                    {
                        if (lastSelectedButton == 3)
                            points++;
                    }
                    else
                    {
                        if (lastSelectedButton == 1)
                            points++;
                    }
                    break;
                case 1:
                    if (esSuma)
                    {
                        if (lastSelectedButton == 0)
                            points++;
                    }
                    else
                    {
                        if (lastSelectedButton == 2)
                            points++;
                    }
                    break;
                case 2:
                    if (esSuma)
                    {
                        if (lastSelectedButton == 2)
                            points++;
                    }
                    else
                    {
                        if (lastSelectedButton == 0)
                            points++;
                    }
                    break;
                case 3:
                    if (esSuma)
                    {
                        if (lastSelectedButton == 3)
                            points++;
                    }
                    else
                    {
                        if (lastSelectedButton == 1)
                            points++;
                        
                    }
                    break;
                default:
                    break;
            }
        }

        Mensajes.GetComponent<TextMeshProUGUI>().text = "Points: " + points;

        lastSelectedButton = -1;
        current_times++;

        this.SetLabels();
    }

    public void GoToGames()
    {
        if (PlayerPrefs.HasKey("Juego3_points"))
        {
            if (PlayerPrefs.GetInt("Juego3_points") < points)
            {
                PlayerPrefs.SetInt("Juego3_points", points);
            }
        }
        else
        {
            PlayerPrefs.SetInt("Juego3_points", points);
        }

        SaveAcumulado();
        SceneManager.LoadScene("Scene1");
    }

    public void SaveAcumulado()
    {
        if (PlayerPrefs.HasKey("Juego3_acumulado"))
        {
            PlayerPrefs.SetInt("Juego3_acumulado", PlayerPrefs.GetInt("Juego3_acumulado") + points);
        }
        else
        {
            PlayerPrefs.SetInt("Juego3_acumulado", points);
        }
    }
}

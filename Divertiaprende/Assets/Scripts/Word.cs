﻿using System;

[Serializable]
public class Word
{
    public int id;
    public string word;
    public string phrase;
}
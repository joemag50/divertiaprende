﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class Juego2_Controller : MonoBehaviour
{
    public GameObject A, B, C, D, Phrase, Mensajes;
    string[] words = { "Fuerte", "Bonito", "Delicado", "Alto" };
    public int lastSelectedButton = -1;
    public GameObject landscape;
    public GameObject buttonNext;

    Adjective[] adjectives;
    int adjectives_size;
    int max_adjectives_per_level;
    int current_question;
    int points;
    Question[] questions_on_level;
    int answer;

    void Awake()
    {
        string response = JsonHelper.fixJson(PlayerPrefs.GetString("ApiResponse"), "adjectives");
        adjectives = JsonHelper.FromJson<Adjective>(response);

        adjectives_size = adjectives.Length;
        max_adjectives_per_level = 10;
        current_question = 0;
        points = 0;

        questions_on_level = new Question[max_adjectives_per_level];

        for (int i = 0; i < max_adjectives_per_level; i++)
        {
            questions_on_level[i] = new Question();

            int r = Random.Range(1, adjectives_size - 2);
            int r2 = Random.Range(0, 3);

            Adjective tmp = adjectives[r];
            Adjective tmp_1 = adjectives[r + 1];
            Adjective tmp_2 = adjectives[r - 1];
            Adjective tmp_3 = adjectives[r + 2];

            switch (r2)
            {
                case 0:
                    questions_on_level[i].A = tmp.adjective;
                    questions_on_level[i].B = tmp_1.adjective;
                    questions_on_level[i].C = tmp_2.adjective;
                    questions_on_level[i].D = tmp_3.adjective;
                    break;
                case 1:
                    questions_on_level[i].A = tmp_1.adjective;
                    questions_on_level[i].B = tmp.adjective;
                    questions_on_level[i].C = tmp_2.adjective;
                    questions_on_level[i].D = tmp_3.adjective;
                    break;
                case 2:
                    questions_on_level[i].A = tmp_1.adjective;
                    questions_on_level[i].B = tmp_2.adjective;
                    questions_on_level[i].C = tmp.adjective;
                    questions_on_level[i].D = tmp_3.adjective;
                    break;
                case 3:
                    questions_on_level[i].A = tmp_1.adjective;
                    questions_on_level[i].B = tmp_2.adjective;
                    questions_on_level[i].C = tmp_3.adjective;
                    questions_on_level[i].D = tmp.adjective;
                    break;
                default:
                    questions_on_level[i].A = tmp.adjective;
                    questions_on_level[i].B = tmp_1.adjective;
                    questions_on_level[i].C = tmp_2.adjective;
                    questions_on_level[i].D = tmp_3.adjective;
                    break;
            }

            questions_on_level[i].answer = r2;
            questions_on_level[i].resource = tmp.landscape;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        this.NextQuestion();
    }

    void NextQuestion()
    {
        if (current_question == max_adjectives_per_level)
        {
            GoToGames();
            return;
        }

        A.GetComponentInChildren<TextMeshProUGUI>().text = questions_on_level[current_question].A;
        B.GetComponentInChildren<TextMeshProUGUI>().text = questions_on_level[current_question].B;
        C.GetComponentInChildren<TextMeshProUGUI>().text = questions_on_level[current_question].C;
        D.GetComponentInChildren<TextMeshProUGUI>().text = questions_on_level[current_question].D;

        answer = questions_on_level[current_question].answer;

        this.SetCoolLandscape(questions_on_level[current_question].resource);
        current_question++;
    }

    public void ButtonWord(int option)
    {
        this.lastSelectedButton = option;
    }

    public void ButtonGo()
    {
        if (lastSelectedButton < 0)
        {
            //JCGE: ??
        }
        else
        {
            if (lastSelectedButton == answer)
                points++;

            Mensajes.GetComponent<TextMeshProUGUI>().text = "Puntos = " + points;
        }
        this.lastSelectedButton = -1;
        this.NextQuestion();
    }

    public void GoToGames()
    {
        if (PlayerPrefs.HasKey("Juego2_points"))
        {
            if (PlayerPrefs.GetInt("Juego2_points") < points)
            {
                PlayerPrefs.SetInt("Juego2_points", points);
            }
        }
        else
        {
            PlayerPrefs.SetInt("Juego2_points", points);
        }

        SaveAcumulado();
        SceneManager.LoadScene("Scene1");
    }

    void SetCoolLandscape(string landscape_url)
    {
        buttonNext.GetComponent<Button>().interactable = false;
        StartCoroutine(DownloadImage(landscape_url));
    }

    IEnumerator DownloadImage(string MediaUrl)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
            Debug.Log(request.error);
        else
            landscape.GetComponent<RawImage>().texture = ((DownloadHandlerTexture)request.downloadHandler).texture;

        buttonNext.GetComponent<Button>().interactable = true;
    }

    public void SaveAcumulado()
    {
        if (PlayerPrefs.HasKey("Juego2_acumulado"))
        {
            PlayerPrefs.SetInt("Juego2_acumulado", PlayerPrefs.GetInt("Juego2_acumulado") + points);
        }
        else
        {
            PlayerPrefs.SetInt("Juego2_acumulado", points);
        }
    }
}

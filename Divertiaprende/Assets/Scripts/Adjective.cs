﻿using System;

[Serializable]
public class Adjective
{
    public int id;
    public string adjective;
    public string landscape;
}

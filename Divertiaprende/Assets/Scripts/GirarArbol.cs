﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GirarArbol : MonoBehaviour
{
    int side = 1;
    float segundo = 5.0f;

    // Update is called once per frame
    void Update()
    {
        segundo = segundo - Time.deltaTime;
        if (segundo < 0)
        {
            segundo = 5.0f;
            side *= -1;
        }
        transform.Rotate(side * new Vector3(0, 0, 3) * Time.deltaTime);
    }

}

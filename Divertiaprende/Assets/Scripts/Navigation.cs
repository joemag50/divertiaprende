﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class Navigation : MonoBehaviour
{
    public GameObject splashScreen,
                      lobbyScreen,
                      personajesScreen,
                      juegosScreen,
                      puntajesScreen,
                      audioScreen,
                      cuentaScreen,
                      avisos_juego;

    public GameObject splashAnimal, lobbyAnimal;
    public Sprite buffalo, oso, pato;

    float splashCounter = 1.5f;

    string lastScreen;
    string currentScreen;
    int animal;

    void Awake()
    {
        if (PlayerPrefs.HasKey("Animal"))
        {
            animal = PlayerPrefs.GetInt("Animal");
        }
        else
        {
            PlayerPrefs.SetInt("Animal", 2);
            animal = PlayerPrefs.GetInt("Animal");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        LoadAnimal("splash");
        avisos_juego.GetComponent<TextMeshProUGUI>().text = "";
        lastScreen = "";
        currentScreen = "splash";
    }

    // Update is called once per frame
    void Update()
    {
        if (splashCounter > -1f)
        {
            splashCounter -= Time.deltaTime;
            if (splashCounter < 0f)
            {
                this.SetScreen("lobby");
            }
        }

        if (Input.GetKeyUp(KeyCode.Escape))
        {
            BackButton();
        }
    }

    public void SetScreen(string screen)
    {
        lastScreen = currentScreen;

        switch (screen)
        {
            case "splash":
                splashScreen.SetActive(true);

                lobbyScreen.SetActive(false);
                personajesScreen.SetActive(false);
                juegosScreen.SetActive(false);
                puntajesScreen.SetActive(false);
                audioScreen.SetActive(false);
                cuentaScreen.SetActive(false);

                LoadAnimal("splash");
                splashCounter = 1.5f;
                break;
            case "lobby":
                lobbyScreen.SetActive(true);

                splashScreen.SetActive(false);
                personajesScreen.SetActive(false);
                juegosScreen.SetActive(false);
                puntajesScreen.SetActive(false);
                audioScreen.SetActive(false);
                cuentaScreen.SetActive(false);

                LoadAnimal("lobby");
                break;
            case "personajes":
                personajesScreen.SetActive(true);

                splashScreen.SetActive(false);
                lobbyScreen.SetActive(false);
                juegosScreen.SetActive(false);
                puntajesScreen.SetActive(false);
                audioScreen.SetActive(false);
                cuentaScreen.SetActive(false);
                break;
            case "juegos":
                juegosScreen.SetActive(true);
                avisos_juego.GetComponent<TextMeshProUGUI>().text = "";

                splashScreen.SetActive(false);
                lobbyScreen.SetActive(false);
                personajesScreen.SetActive(false);
                puntajesScreen.SetActive(false);
                audioScreen.SetActive(false);
                cuentaScreen.SetActive(false);
                break;
            case "puntajes":
                puntajesScreen.SetActive(true);

                splashScreen.SetActive(false);
                lobbyScreen.SetActive(false);
                personajesScreen.SetActive(false);
                juegosScreen.SetActive(false);
                audioScreen.SetActive(false);
                cuentaScreen.SetActive(false);
                break;
            case "audio":
                audioScreen.SetActive(true);

                splashScreen.SetActive(false);
                lobbyScreen.SetActive(false);
                personajesScreen.SetActive(false);
                juegosScreen.SetActive(false);
                puntajesScreen.SetActive(false);
                cuentaScreen.SetActive(false);
                break;
            case "cuenta":
                cuentaScreen.SetActive(true);

                splashScreen.SetActive(false);
                lobbyScreen.SetActive(false);
                personajesScreen.SetActive(false);
                juegosScreen.SetActive(false);
                puntajesScreen.SetActive(false);
                audioScreen.SetActive(false);
                break;
            default:
                break;
        }

        currentScreen = screen;
    }

    public void GoToGame(int game)
    {
        avisos_juego.GetComponent<TextMeshProUGUI>().text = "";

        if (PlayerPrefs.HasKey("ApiResponse"))
        {
            if (game == 2)
            {
                if (CheckInternetConnection())
                {
                    SceneManager.LoadScene("Game2");
                }
                else
                {
                    avisos_juego.GetComponent<TextMeshProUGUI>().text = "Revisa tu conexión a internet";
                }
            }
            else
            {
                SceneManager.LoadScene("Game" + game);
            }
        }
    }

    public void BackButton()
    {
        SetScreen(lastScreen);
    }

    public void CloseGame()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
            activity.Call<bool>("moveTaskToBack", true);
        }
        else
        {
            Application.Quit();
        }
    }

    public bool CheckInternetConnection()
    {
        return !(Application.internetReachability == NetworkReachability.NotReachable);
    }

    public void SaveAnimal(int animal_local)
    {
        PlayerPrefs.SetInt("Animal", animal_local);
        this.animal = PlayerPrefs.GetInt("Animal");

        SetScreen("splash");
    }

    public void LoadAnimal(string where)
    {
        switch(where)
        {
            case "splash":
                splashAnimal.GetComponent<SpriteRenderer>().sprite = GetAnimal();
                break;
            case "lobby":
                lobbyAnimal.GetComponent<SpriteRenderer>().sprite = GetAnimal();
                break;
            default:
                break;
        }
    }

    private Sprite GetAnimal()
    {
        Sprite result = oso;

        switch(this.animal)
        {
            case 0:
                result = buffalo;
                break;
            case 1:
                result = oso;
                break;
            case 2:
                result = pato;
                break;
            default:
                break;
        }

        return result;
    }
}
